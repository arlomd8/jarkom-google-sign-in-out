# JarKom Google Sign In Out

NAMA : **ARLO MARIO DENDI**

NRP  : **4210181018**

**MATA KULIAH PRAKTIKUM JARINGAN KOMPUTER II**

**TUGAS 5 - PRAKTIKUM GOOGLE SIGN IN AND SIGN OUT**

**DESCRIPTION** 

1.  Open with http://localhost/JARKOM%20SIGN%20IN%20PROJECT/JARKOM%20SIGN%20IN.html
2.  Program will prompt user to sign in with google account
2.  If success the display will pop up a notification "Sign in success."
3.  The program will display the basic information of the google user such name, image, and email.
4.  When user press the sign out button, the user will be signed out.
5.  You can check the Console.Log in inspect element->console
