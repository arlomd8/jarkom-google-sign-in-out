
function onSignIn(googleUser)
{
 var profile = googleUser.getBasicProfile();
	
	//Get information
 	$(".g-signin2").css("display", "none");
	$(".data").css("display", "block");
	$("#pic").attr('src', profile.getImageUrl());
	$("email").text(profile.getEmail());
	 
	//Get information and print in console
	console.log("ID: " + profile.getId()); 
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());
    var id_token = googleUser.getAuthResponse().id_token;
	console.log("ID Token: " + id_token);

	//Print the user who login
	window.alert("Sign in success. ");
	document.getElementById("nameProfile").innerHTML = profile.getName();
	document.getElementById("emailProfile").innerHTML = profile.getEmail();

}

//From the documentation: https://developers.google.com/identity/sign-in/web/sign-in#sign_out_a_user,
//The signOut() method enables users to sign out of your app without signing out of Google.
function signOut()
{
 var auth2 = gapi.auth2.getAuthInstance();
 auth2.signOut().then(function()
 {
	alert("Sign out success. ");
  
	$(".g-signin2").css("display", "block");
	$(".data").css("display", "none"); 
	console.log('User signed out.');
	
 })
 auth2.disconnect();
}